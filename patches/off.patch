From ebe36b423740c38aea24ad6d380b58a2e4a956bf Mon Sep 17 00:00:00 2001
From: imoseyon <imoseyon@gmail.com>
Date: Wed, 18 Nov 2015 22:29:21 -0800
Subject: [PATCH] cpufreq: interactive: add screen off max frequency tunable

@nathanchance: updated to current interactive from this commit: https://github.com/imoseyon/leanKernel-angler/commit/465224c1ed4d9686aaabdb7e176ea70a65ae1295

Signed-off-by: Nathan Chancellor <natechancellor@gmail.com>
Signed-off-by: Harsh Shandilya <harsh@prjkt.io>
---
 drivers/cpufreq/cpufreq_interactive.c | 41 +++++++++++++++++++++++++++++++++++
 1 file changed, 41 insertions(+)

diff --git a/drivers/cpufreq/cpufreq_interactive.c b/drivers/cpufreq/cpufreq_interactive.c
index 9351a790aa37..7cda66637fae 100644
--- a/drivers/cpufreq/cpufreq_interactive.c
+++ b/drivers/cpufreq/cpufreq_interactive.c
@@ -31,6 +31,7 @@
 #include <linux/workqueue.h>
 #include <linux/kthread.h>
 #include <linux/slab.h>
+#include <linux/display_state.h>
 
 #define CREATE_TRACE_POINTS
 #include <trace/events/cpufreq_interactive.h>
@@ -171,6 +172,10 @@ struct cpufreq_interactive_tunables {
 
 	/* Improves frequency selection for more energy */
 	bool powersave_bias;
+
+	/* Maximum frequency while the screen is off */
+#define DEFAULT_SCREEN_OFF_MAX 307200
+	unsigned long screen_off_max;
 };
 
 /* For cases where we have single governor instance for system */
@@ -748,6 +753,11 @@ static int cpufreq_interactive_speedchange_task(void *data)
 				continue;
 			}
 
+			if (unlikely(!display_on)) {
+			    if (ppol->target_freq > tunables->screen_off_max)
+				ppol->target_freq = tunables->screen_off_max;
+			}
+
 			if (ppol->target_freq != ppol->policy->cur) {
 			    if (tunables->powersave_bias || !display_on)
 				    __cpufreq_driver_target(ppol->policy,
@@ -1486,6 +1496,32 @@ static ssize_t store_powersave_bias(struct cpufreq_interactive_tunables *tunable
 	return count;
 }
 
+static ssize_t show_screen_off_maxfreq(
+		struct cpufreq_interactive_tunables *tunables,
+                char *buf)
+{
+	return sprintf(buf, "%lu\n", tunables->screen_off_max);
+}
+
+static ssize_t store_screen_off_maxfreq(
+		struct cpufreq_interactive_tunables *tunables,
+                const char *buf, size_t count)
+{
+	int ret;
+	unsigned long val;
+
+	ret = kstrtoul(buf, 0, &val);
+	if (ret < 0)
+		return ret;
+
+	if (val < 384000)
+		tunables->screen_off_max = DEFAULT_SCREEN_OFF_MAX;
+	else
+		tunables->screen_off_max = val;
+
+	return count;
+}
+
 /*
  * Create show/store routines
  * - sys: One governor instance for complete SYSTEM
@@ -1542,6 +1578,7 @@ show_store_gov_pol_sys(ignore_hispeed_on_notif);
 show_store_gov_pol_sys(fast_ramp_down);
 show_store_gov_pol_sys(enable_prediction);
 show_store_gov_pol_sys(powersave_bias);
+show_store_gov_pol_sys(screen_off_maxfreq);
 
 #define gov_sys_attr_rw(_name)						\
 static struct global_attr _name##_gov_sys =				\
@@ -1574,6 +1611,7 @@ gov_sys_pol_attr_rw(ignore_hispeed_on_notif);
 gov_sys_pol_attr_rw(fast_ramp_down);
 gov_sys_pol_attr_rw(enable_prediction);
 gov_sys_pol_attr_rw(powersave_bias);
+gov_sys_pol_attr_rw(screen_off_maxfreq);
 
 static struct global_attr boostpulse_gov_sys =
 	__ATTR(boostpulse, 0200, NULL, store_boostpulse_gov_sys);
@@ -1603,6 +1641,7 @@ static struct attribute *interactive_attributes_gov_sys[] = {
 	&fast_ramp_down_gov_sys.attr,
 	&enable_prediction_gov_sys.attr,
 	&powersave_bias_gov_sys.attr,
+	&screen_off_maxfreq_gov_sys.attr,
 	NULL,
 };
 
@@ -1633,6 +1672,7 @@ static struct attribute *interactive_attributes_gov_pol[] = {
 	&fast_ramp_down_gov_pol.attr,
 	&enable_prediction_gov_pol.attr,
 	&powersave_bias_gov_pol.attr,
+	&screen_off_maxfreq_gov_pol.attr,
 	NULL,
 };
 
@@ -1671,6 +1711,7 @@ static struct cpufreq_interactive_tunables *alloc_tunable(
 	tunables->timer_rate = usecs_to_jiffies(DEFAULT_TIMER_RATE);
 	tunables->boostpulse_duration_val = DEFAULT_MIN_SAMPLE_TIME;
 	tunables->timer_slack_val = usecs_to_jiffies(DEFAULT_TIMER_SLACK);
+	tunables->screen_off_max = DEFAULT_SCREEN_OFF_MAX;
 
 	spin_lock_init(&tunables->target_loads_lock);
 	spin_lock_init(&tunables->above_hispeed_delay_lock);

