#!/bin/bash
kernel_version="New_Tree"
kernel_name="Renice"
device_name="Z2_Plus"
zip_name="$kernel_name-$device_name-$kernel_version.zip"

# ccache
export USE_CCACHE=1
export CCACHE_DIR=/home/adeveloper79/ccache/renice

export HOME="/home"
export CONFIG_FILE="z2_plus_defconfig"
export ARCH="arm64"
export KBUILD_BUILD_USER="adeveloper79"
export KBUILD_BUILD_HOST="Predator-G3-572"
export TOOLCHAIN_PATH="${HOME}/adeveloper79/toolchain/UBERTC-aarch64-linux-android-5.3/bin"
export CROSS_COMPILE=$TOOLCHAIN_PATH/aarch64-linux-android-
export CONFIG_ABS_PATH="arch/${ARCH}/configs/${CONFIG_FILE}"
export objdir="$HOME/adeveloper79/kernel/k/out"
export sourcedir="$HOME/adeveloper79/kernel/z2"
export anykernel="$HOME/adeveloper79/kernel/k/zipping"
compile() {
  make O=$objdir  $CONFIG_FILE -j16
  make O=$objdir -j16
}
clean() {
  make O=$objdir CROSS_COMPILE=${CROSS_COMPILE}  $CONFIG_FILE -j16
  make O=$objdir mrproper
  make O=$objdir clean
}
#module_stock(){
#  rm -rf $anykernel/modules/
#  mkdir $anykernel/modules
#  find $objdir -name '*.ko' -exec cp -av {} $anykernel/modules/ \;
  # strip modules
#  ${CROSS_COMPILE}strip --strip-unneeded $anykernel/modules/*
#  cp -rf $objdir/arch/$ARCH/boot/Image.gz-dtb $anykernel/zImage
#}
delete_zip(){
  cd $anykernel
  find . -name "*.zip" -type f
  find . -name "*.zip" -type f -delete
}
build_package(){
  cp -rf $objdir/arch/$ARCH/boot/Image.gz-dtb $anykernel/zImage
  zip -r9 UPDATE-AnyKernel2.zip * -x README UPDATE-AnyKernel2.zip
}
make_name(){
  mv UPDATE-AnyKernel2.zip $zip_name
}
turn_back(){
cd $sourcedir
}
compile
#module_stock
delete_zip
build_package
make_name
turn_back
